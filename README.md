# transproc
[![pipeline status](https://gitlab.com/micuda-python/transproc/badges/master/pipeline.svg)](https://gitlab.com/micuda-python/transproc/-/commits/master)
[![coverage report](https://gitlab.com/micuda-python/transproc/badges/master/coverage.svg)](https://gitlab.com/micuda-python/transproc/-/commits/master)

For documentation see tests: [https://gitlab.com/micuda-python/transproc/-/tree/master/tests](https://gitlab.com/micuda-python/transproc/-/tree/master/tests)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [https://gitlab.com/micuda-python/transproc/-/issues](https://gitlab.com/micuda-python/transproc/-/issues)

## Requirements for builds

Install requirements for setup beforehand using

```bash
poetry install -E test -E build
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## License

Distributed under the MIT License. See `LICENSE` for more information.
